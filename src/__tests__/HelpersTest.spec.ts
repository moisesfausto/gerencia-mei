import { knownRequestError } from '@helpers/requestErros'
import {PrismaClientErrors} from '../app/errors/ClientErrors'
describe('knownRequestError', function () {
	it('should to do replaced of the target', function () {
		const { code, message } = knownRequestError(PrismaClientErrors, 'P2002', { target: 'id' })

		expect(code).toEqual(500)
		expect(message).toBe('Unique constraint failed on the id')
	})

	it('should to do replaced of the field_name', function () {
		const { code, message } = knownRequestError(PrismaClientErrors, 'P2003', { field_name: 'email' })

		expect(code).toEqual(500)
		expect(message).toBe('Foreign key constraint failed on the field: email')
	})

	it('should to do cause of the cause', function () {
		const { code, message } = knownRequestError(PrismaClientErrors, 'P2025', { cause: 'A cause whatever' })

		expect(code).toEqual(500)
		expect(message).toBe('A cause whatever')
	})
})
