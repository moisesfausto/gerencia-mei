import bcrypt from 'bcryptjs'
import { ClienteModel } from '../app/database'
import {knownRequestError} from '@helpers/requestErros'
import {PrismaClientErrors} from '../app/errors/ClientErrors'
import UserRepositories from '@repositories/UserRepositories'
import { v4 as uuid } from 'uuid'

jest.mock('../app/database', () => ({
	ClienteModel: {
		user: {
			findMany: jest.fn(() => Promise.resolve([
				{
					id: uuid(),
					account_google_id: uuid(),
					name: 'John Doe',
					email: 'johndoe@example.com',
				},
				{
					id: uuid(),
					account_google_id: uuid(),
					name: 'Jane Smith',
					email: 'janesmith@example.com',
				},
			])),
			findFirst: jest.fn(() => Promise.resolve({
				id: uuid(),
				account_google_id: uuid(),
				name: 'Jane Smith',
				email: 'janesmith@example.com',
			})),
			create: jest.fn(() => Promise.resolve({
				id: '044d5764-8967-4490-964c-a6a87da22cbe',
				account_google_id: '50e78472-e73f-4a5d-b231-657b411745b7',
				name: 'John Doe',
				email: 'johndoe@example.com',
			})),
			update: jest.fn(() => Promise.resolve({
				id: '044d5764-8967-4490-964c-a6a87da22cbe',
				account_google_id: '50e78472-e73f-4a5d-b231-657b411745b7',
				name: 'John Doe',
				email: 'johndoe@example.com',
			})),
			delete: jest.fn(() => Promise.resolve({
				message: 'User deleted with success'
			})),
		},
	},
}))

describe('UserRepositories', () => {
	describe('findAll', () => {
		it('should return all users', async () => {
			const response = await UserRepositories.findAll()

			expect(ClienteModel.user.findMany).toHaveBeenCalled()
			expect(response.length).toBeGreaterThanOrEqual(1)
			expect(response[0]).toHaveProperty('id')
		})

		it('should return array empty', async () => {
			const findManyMock = jest.spyOn(ClienteModel.user, 'findMany')
			findManyMock.mockResolvedValue([])

			const response = await UserRepositories.findAll()

			expect(response).toEqual([])
		})
	})

	describe('findById', () => {
		it('should return an user', async () => {
			const response = await UserRepositories.findById(uuid())

			expect(ClienteModel.user.findFirst).toHaveBeenCalled()
			expect(response).toHaveProperty('id')
		})

		it('should return message user not found', async () => {
			const findFirstMock = jest.spyOn(ClienteModel.user, 'findFirst')
			findFirstMock.mockResolvedValue(null)

			const response = await UserRepositories.findById('uuidv4')

			expect(response).toBe(null)
		})
	})

	describe('create', () => {
		it('should create a new user', async () => {
			const createUserMock = jest.spyOn(ClienteModel.user, 'create')
			const passwordHash = await bcrypt.hash('password123', 10)

			createUserMock.mockResolvedValue({
				id: '044d5764-8967-4490-964c-a6a87da22cbe',
				account_google_id: '50e78472-e73f-4a5d-b231-657b411745b7',
				name: 'John Doe',
				email: 'johndoe@example.com',
				password: passwordHash,
				createdAt: new Date('2023-08-30 03:21:15.179'),
				updatedAt: new Date('2023-09-12 02:36:47.546'),
			})

			const user = {
				account_google_id: '50e78472-e73f-4a5d-b231-657b411745b7',
				name: 'John Doe',
				email: 'johndoe@example.com',
				password: 'password123',
			}

			const response = await UserRepositories.create(user)

			expect(ClienteModel.user.create).toHaveBeenCalled()
			expect(response).toEqual({
				id: '044d5764-8967-4490-964c-a6a87da22cbe',
				account_google_id: '50e78472-e73f-4a5d-b231-657b411745b7',
				name: 'John Doe',
				email: 'johndoe@example.com',
				password: passwordHash,
				createdAt: new Date('2023-08-30 03:21:15.179'),
				updatedAt: new Date('2023-09-12 02:36:47.546'),
			})
		})

		it('should handle know request errors', async () => {
			const createUserMock = jest.spyOn(ClienteModel.user, 'create')

			createUserMock.mockRejectedValue({
				code: 500,
				message: 'Internal Server Error',
			})

			const user = {
				account_google_id: '1234',
				name: 'John Doe',
				email: 'johndoe@example.com',
				password: 'password123',
			}

			try {
				await UserRepositories.create(user)
			} catch (e: any) {
				const { code, message } = knownRequestError(PrismaClientErrors, e.code, e.meta)

				expect(code).toEqual(500)
				expect(message).toBe('Internal Server Error')
			}
		})
	})

	describe('update', () => {
		it('should update an user already exists', async () => {
			const updateUseMock = jest.spyOn(ClienteModel.user, 'update')
			const passwordHash = await bcrypt.hash('password123', 10)

			updateUseMock.mockResolvedValue({
				id: '044d5764-8967-4490-964c-a6a87da22cbe',
				account_google_id: '50e78472-e73f-4a5d-b231-657b411745b7',
				name: '[update] John Doe',
				email: 'johndoe@example.com',
				password: passwordHash,
				createdAt: new Date('2023-08-30 03:21:15.179'),
				updatedAt: new Date('2023-09-12 02:36:47.546'),
			})

			const user = {
				id: '1',
				account_google_id: '123',
				name: '[update] John Doe',
				email: 'johndoe@example.com',
				password: 'new_password',
			}

			const response = await UserRepositories.update(user)

			expect(updateUseMock).toHaveBeenCalled()
			expect(response).toEqual({
				id: '044d5764-8967-4490-964c-a6a87da22cbe',
				account_google_id: '50e78472-e73f-4a5d-b231-657b411745b7',
				name: '[update] John Doe',
				email: 'johndoe@example.com',
				password: passwordHash,
				createdAt: new Date('2023-08-30 03:21:15.179'),
				updatedAt: new Date('2023-09-12 02:36:47.546'),
			})
		})

		it('should handle know request errors', async () => {
			const updateUserMock = jest.spyOn(ClienteModel.user, 'update')

			updateUserMock.mockRejectedValue({
				code: 500,
				message: 'Internal Server Error',
			})

			const user = {
				account_google_id: '1234',
				name: 'John Doe',
				email: 'johndoe@example.com',
				password: 'password123',
			}

			try {
				await UserRepositories.update(user)
			} catch (e: any) {
				const { code, message } = knownRequestError(PrismaClientErrors, e?.code, e?.meta)

				expect(code).toEqual(500)
				expect(message).toBe('Internal Server Error')
			}
		})
	})

	describe('delete',() => {
		it('should delete an user', async () => {
			const deleteUserMock = jest.spyOn(ClienteModel.user, 'delete')
			const passwordHash = await bcrypt.hash('password123', 10)

			deleteUserMock.mockResolvedValue({
				id: '044d5764-8967-4490-964c-a6a87da22cbe',
				account_google_id: '50e78472-e73f-4a5d-b231-657b411745b7',
				name: '[update] John Doe',
				email: 'johndoe@example.com',
				password: passwordHash,
				createdAt: new Date('2023-08-30 03:21:15.179'),
				updatedAt: new Date('2023-09-12 02:36:47.546'),
			})

			await UserRepositories.delete('044d5764-8967-4490-964c-a6a87da22cbe')

			expect(deleteUserMock).toHaveBeenCalled()
		})

		it('should return error to delete an user', async () => {
			const deleteUserMock = jest.spyOn(ClienteModel.user, 'delete')

			deleteUserMock.mockRejectedValue({
				code: 500,
				message: 'Internal Server Error',
			})

			try {
				await UserRepositories.delete('044d5764-8967-4490-964c-a6a87da22cbe')
			} catch (e: any) {
				const { code, message } = knownRequestError(PrismaClientErrors, e?.code, e?.meta)

				expect(code).toEqual(500)
				expect(message).toBe('Internal Server Error')
			}
		})
	})
})
