import multer from '@config/multer'
import Router from 'express'

import CompanyController from '@controllers/CompanyController'
import DasCompanyController from '@controllers/DasCompanyController'
import UserController from '@controllers/UserController'

const router = Router()

// User
router.get('/user', UserController.index)
router.post('/user', UserController.store)
router.get('/user/:id', UserController.show)
router.put('/user/:id', UserController.update)
router.delete('/user/:id', UserController.delete)

// Company
router.get('/company', CompanyController.index)
router.post('/company', CompanyController.store)
router.get('/company/:id', CompanyController.show)
router.put('/company/:id', CompanyController.update)
router.delete('/company/:id', CompanyController.delete)

// Das Company
router.get('/das-company', DasCompanyController.index)
router.post('/das-company', DasCompanyController.store)
router.get('/das-company/:id', DasCompanyController.show)
router.put('/das-company/:id', DasCompanyController.update)
router.delete('/das-company/:id', DasCompanyController.delete)
// Storage Google
router.post('/das-company-storage/upload', multer.single('attachment'), DasCompanyController.upload)
router.get('/das-company-storage/list/:id_user/:id_company', DasCompanyController.list)
router.get('/das-company-storage/download/:id_user/:id_company/:file', DasCompanyController.download)
router.delete('/das-company-storage/delete/:id_user/:id_company/:file', DasCompanyController.remove)

export { router }
