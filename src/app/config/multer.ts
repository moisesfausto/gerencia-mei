import Multer from 'multer'

const multer = Multer({
	limits: {
		fileSize: 2 * 1024 * 1024
	},
	fileFilter: (req, file, cb) => {
		const allowedMimes = ['application/pdf']

		if (allowedMimes.includes(file.mimetype)) cb(null, true)
		else cb(new Error('Invalid file type'))
	}
})

export default multer
