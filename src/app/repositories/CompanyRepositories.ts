import { ClienteModel } from '../database'
import { knownRequestError } from '@helpers/requestErros'
import { PrismaClientErrors } from '../errors/ClientErrors'

class CompanyRepositories {
	async findAll(orderBy: any = 'asc') {
		const companies = await ClienteModel.company.findMany({
			select: {
				id: true,
				user_id: true,
				cnpj: true,
				answerable: true,
				type: true,
				section: true
			},
			orderBy: {
				id: orderBy
			}
		})

		return companies
	}

	async findById(id: string) {
		const company = await ClienteModel.company.findFirst({
			where: {
				id
			},
			select: {
				id: true,
				user_id: true,
				cnpj: true,
				answerable: true,
				type: true,
				section: true
			}
		})

		return company
	}

	async create({user_id, cnpj, answerable, type, section}: ICompany): Promise<object | IError | any> {
		try {
			const company = await ClienteModel.company.create({
				data: {
					user_id,
					cnpj,
					answerable,
					type,
					section
				},
				select: {
					id: true,
					user_id: true,
					cnpj: true,
					answerable: true,
					type: true,
					section: true
				}
			})

			return company
		} catch (error: any) {
			return knownRequestError(PrismaClientErrors, error?.code, error?.meta)
		}

	}

	async update({id, answerable, type, section}: ICompany): Promise<object | IError | any> {
		try {
			const company = await ClienteModel.company.update({
				where: {
					id
				},
				data: {
					answerable,
					type,
					section
				},
				select: {
					id: true,
					user_id: true,
					cnpj: true,
					answerable: true,
					type: true,
					section: true
				}
			})

			return company
		} catch (error: any) {
			return knownRequestError(PrismaClientErrors, error?.code, error?.meta)
		}
	}

	async delete(id: string): Promise<object | IError | any> {
		try {
			await ClienteModel.company.delete({
				where: {
					id
				},
				include: {
					das_companies: true
				}
			})
		} catch (error: any) {
			return knownRequestError(PrismaClientErrors, error?.code, error?.meta)
		}
	}
}

export default new CompanyRepositories()
