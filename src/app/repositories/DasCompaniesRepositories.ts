import { ClienteModel } from '../database'
import { formatToDateTime } from '@utils/utils'
import { knownRequestError } from '@helpers/requestErros'
import { PrismaClientErrors } from '../errors/ClientErrors'

class DasCompaniesRepositories {
	async findAll(orderBy: any = 'asc') {
		const dasCompany = await ClienteModel.dasCompany.findMany({
			select: {
				id: true,
				month: true,
				year: true,
				due_date: true,
				value: true,
				amount_paid: true,
				situation: true,
				attachment: true,
			},
			orderBy: {
				id: orderBy
			}
		})

		return dasCompany
	}

	async findById(id: string) {
		const dasCompany = await ClienteModel.dasCompany.findFirst({
			where: {
				id
			},
			select: {
				id: true,
				month: true,
				year: true,
				due_date: true,
				value: true,
				amount_paid: true,
				situation: true,
				attachment: true,
			}
		})

		return dasCompany
	}

	async create({ id_company, month, year, due_date, value, amount_paid, situation, attachment }: IDasCompany): Promise<object | IError | any> {
		try {
			const dasCompany = await ClienteModel.dasCompany.create({
				data: {
					id_company,
					month,
					year,
					due_date: formatToDateTime(due_date),
					value,
					amount_paid,
					situation,
					attachment
				},
				select: {
					month: true,
					year: true,
					due_date: true,
					value: true,
					amount_paid: true,
					situation: true,
					attachment: true,
				}
			})

			return dasCompany
		} catch (error: any) {
			return knownRequestError(PrismaClientErrors, error?.code, error?.meta)
		}
	}

	async update({id,  month, year, due_date, value, amount_paid, situation, attachment}: IDasCompany): Promise<object | IError | any> {
		try {
			const dasCompany = await ClienteModel.dasCompany.update({
				where: {
					id
				},
				data: {
					month,
					year,
					due_date: formatToDateTime(due_date),
					value,
					amount_paid,
					situation,
					attachment
				},
				select: {
					month: true,
					year: true,
					due_date: true,
					value: true,
					amount_paid: true,
					situation: true,
					attachment: true,
				}
			})

			return dasCompany
		} catch (error: any) {
			return knownRequestError(PrismaClientErrors, error?.code, error?.meta)
		}
	}

	async delete(id: string): Promise<object | IError | any> {
		try {
			await ClienteModel.dasCompany.delete({
				where: {
					id
				}
			})
		} catch (error: any) {
			return knownRequestError(PrismaClientErrors, error?.code, error?.meta)
		}
	}
}

export default new DasCompaniesRepositories()
