import bcrypt from 'bcryptjs'
import { ClienteModel } from '../database'
import { knownRequestError } from '@helpers/requestErros'
import { PrismaClientErrors } from '../errors/ClientErrors'

class UserRepositories {
	async findAll() {
		const users = await ClienteModel.user.findMany({
			select: {
				id: true,
				account_google_id: true,
				name: true,
				email: true,
			},
			orderBy: {
				id: 'asc'
			}
		})

		return users
	}

	async findById(id: string) {
		const user = await ClienteModel.user.findFirst({
			where: {
				id
			},
			select: {
				id: true,
				account_google_id: true,
				name: true,
				email: true,
			}
		})

		return user
	}

	async create({account_google_id, name, email, password}: UserI): Promise<object | IError | any> {
		try {
			const user = await ClienteModel.user.create({
				data: {
					account_google_id: account_google_id,
					name: name,
					email: email,
					password: await bcrypt.hash(password, 10)
				},
				select: {
					id: true,
					account_google_id: true,
					name: true,
					email: true,
				}
			})

			return user
		} catch (error: any) {
			return knownRequestError(PrismaClientErrors, error?.code, error?.meta)
		}

	}

	async update({id, name, password}: UserI): Promise<object | IError | any> {
		try {
			const user = await ClienteModel.user.update({
				where: {
					id
				},
				data: {
					name,
					password: await bcrypt.hash(password, 10)
				},
				select: {
					id: true,
					account_google_id: true,
					name: true,
					email: true,
				}
			})

			return user
		} catch (error: any) {
			return knownRequestError(PrismaClientErrors, error?.code, error?.meta)
		}
	}

	async delete(id: string): Promise<object | IError | any> {
		try {
			await ClienteModel.user.delete({
				where: {
					id
				}
			})
		} catch (error: any) {
			return knownRequestError(PrismaClientErrors, error?.code, error?.meta)
		}
	}
}

export default new UserRepositories()
