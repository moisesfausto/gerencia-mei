interface IClientErrors {
  [key: string]: string
}

const knownRequestError = (json: IClientErrors, property: string, value: any): IError => {
	if (!json || !property) {
		return {
			code: 500,
			message: 'Internal Server Error'
		}
	}

	const { target, field_name, cause } = value

	const propertyValue: string = json[property]

	let replaced = ''

	if (target) replaced = propertyValue.replace(/\{.*?}/g, target)

	if (field_name) replaced = propertyValue.replace(/\{.*?}/g, field_name)

	if (cause) replaced = cause

	return {
		code: 500,
		message: replaced
	}
}

export { knownRequestError }
