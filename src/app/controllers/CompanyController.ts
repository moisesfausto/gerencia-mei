import CompanyRepositories from '@repositories/CompanyRepositories'
import {HandleErrors, NotFoundError} from '@helpers/HandleErrors'
import { NextFunction, Request, Response } from 'express'

class CompanyController {

	/**
   * @description Lista os todos as empresas
   * @param request
   * @param response
   * @returns companies || []
   */
	async index(request: Request, response: Response) {
		const { orderBy } = request.params

		const companies = await CompanyRepositories.findAll(orderBy)

		return response.json(companies)
	}

	/**
	 * @description Mostra uma empresa passado por id
	 * @param request
	 * @param response
	 * @param next
	 * @returns company || {}
	 */
	async show(request: Request, response: Response, next: NextFunction) {
		const { id } = request.params

		const company = await CompanyRepositories.findById(id)

		if (!company) return next(new NotFoundError('Company not found'))

		return response.json(company)
	}

	/**
	 * @description Salva uma novo empresa no banco
	 * @param request
	 * @param response
	 * @param next
	 * @returns company || error
	 */
	async store(request: Request, response: Response, next: NextFunction) {
		const body: ICompany = request.body

		const company = await CompanyRepositories.create(body)

		if (company.code) return next(new HandleErrors(company.message, company.code))

		return response.status(201).json(company)
	}

	/**
	 * @description Atualiza uma empresa
	 * @param request
	 * @param response
	 * @param next
	 * @returns
	 */
	async update(request: Request, response: Response, next: NextFunction) {
		const body: ICompany = {
			id: request.params.id,
			...request.body
		}

		const company = await CompanyRepositories.update(body)

		if (company.code) return next(new HandleErrors(company.message, company.code))

		return response.status(201).json(company)
	}

	/**
	 * @description Deleta o empresa do banco
	 * @param request
	 * @param response
	 * @param next
	 * @returns
	 */
	async delete(request: Request, response: Response, next: NextFunction) {
		const { id } = request.params

		const company = await CompanyRepositories.delete(id)

		if (!company) return response.status(200).json({'message': 'Company deleted with success'})

		return next(new HandleErrors(company.message, company.code))
	}
}

export default new CompanyController()
