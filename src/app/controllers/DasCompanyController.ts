import DasCompaniesRepositories from '@repositories/DasCompaniesRepositories'
import FirebaseServices from '@services/FirebaseServices'
import {HandleErrors, NotFoundError} from '@helpers/HandleErrors'
import { NextFunction, Request, Response } from 'express'

class DasCompany {

	/**
   * @description Lista os todos os DAS
   * @param request
   * @param response
   * @returns DAS
   */
	async index(request: Request, response: Response) {
		const { orderBy } = request.params

		const dasCompany = await DasCompaniesRepositories.findAll(orderBy)

		return response.json(dasCompany)
	}

	/**
	 * @description Mostra um DAS passado por id
	 * @param request
	 * @param response
	 * @param next
	 * @returns DAS
	 */
	async show(request: Request, response: Response, next: NextFunction) {
		const { id } = request.params

		const dasCompany = await DasCompaniesRepositories.findById(id)

		if (!dasCompany) return next(new NotFoundError('User not found'))

		return response.json(dasCompany)
	}

	/**
	 * @description Salva um novo documento DAS no banco
	 * @param request
	 * @param response
	 * @param next
	 * @returns
	 */
	async store(request: Request, response: Response, next: NextFunction) {
		const body: IDasCompany = request.body

		const dasCompany = await DasCompaniesRepositories.create(body)

		if (dasCompany.code) return next(new HandleErrors(dasCompany.message, dasCompany.code))

		return response.status(201).json(dasCompany)
	}

	/**
	 * @description Atualiza um DAS
	 * @param request
	 * @param response
	 * @param next
	 * @returns
	 */
	async update(request: Request, response: Response, next: NextFunction) {
		const body: IDasCompany = {
			id: request.params.id,
			...request.body
		}

		const dasCompany = await DasCompaniesRepositories.update(body)

		if (dasCompany.code) return next(new HandleErrors(dasCompany.message, dasCompany.code))

		return response.status(201).json(dasCompany)
	}

	/**
	 * @description Deleta o documento DAS do banco
	 * @param request
	 * @param response
	 * @param next
	 * @returns
	 */
	async delete(request: Request, response: Response, next: NextFunction) {
		const { id } = request.params

		const dasCompany = await DasCompaniesRepositories.delete(id)

		if (!dasCompany) return response.status(200).json({'message': 'Das Company deleted with success'})

		return next(new HandleErrors(dasCompany.message, dasCompany.code))
	}

	/**
	 * Faz upload de arquivo ao Storage do Google
	 * @param request
	 * @param response
	 */
	async upload(request: Request, response: Response) {
		const {id_user: idUser, id_company: idCompany} = request.body

		try {
			const uploadDasMei = await FirebaseServices.uploadFile(idUser, idCompany, 'das_mei', <IDasCompanyFile>request.file)
			response.status(201).json(uploadDasMei)
		} catch (error) {
			response.status(500).json({message: error})
		}
	}

	/**
	 * @description
	 * @param request
	 * @param response
	 * @returns
	 */
	async download(request: Request, response: Response) {
		const {id_user: idUser, id_company: idCompany, file} = request.params

		const dasMei = await FirebaseServices.downloadFile(idUser, idCompany, 'das_mei', file)

		response.status(200).json({
			download: dasMei
		})
	}

	/**
	 * @description Para listar maiores use list paginado
	 * @param request
   * @param response
	 * @returns Retorna uma lista de arquivos DAS MEI do Storage
	 */
	async list(request: Request, response: Response) {
		const {id_user: idUser, id_company: idCompany} = request.params

		const dasMei = await FirebaseServices.listFile(idUser, idCompany, 'das_mei')

		response.status(200).json(dasMei)
	}

	/**
	 * @description Usado para ditetórios pequenos
	 * @returns
	 */
	listAll() {
		return 'list all'
	}

	/**
	 * @description Deleta arquivo
	 * @param request
	 * @param response
	 */
	async remove(request: Request, response: Response) {
		const {id_user: idUser, id_company: idCompany, file} = request.params

		const dasMei = await FirebaseServices.deleteFile(idUser, idCompany, 'das_mei', file)

		response.status(200).json(dasMei)
	}
}

export default new DasCompany()
