import UserRepositories from '@repositories/UserRepositories'
import {HandleErrors, NotFoundError} from '@helpers/HandleErrors'
import {NextFunction, Request, Response} from 'express'

// https://firebase.google.com/docs/auth/web/start?hl=pt-br

class UserController
{
	/**
   *  Lista os todos os usuários
   * @param request
   * @param response
   * @returns users
   */
	async index(request: Request, response: Response) {
		const users = await UserRepositories.findAll()

		return response.json(users)
	}

	/**
	 * Mostra um usuário passado por id
	 * @param request
	 * @param response
	 * @param next
	 * @returns user
	 */
	async show(request: Request, response: Response, next: NextFunction) {
		const { id } = request.params

		const user = await UserRepositories.findById(id)

		if (!user) return next(new NotFoundError('User not found'))

		return response.status(200).json(user)
	}

	/**
	 * Salva um novo usuário no banco
	 * @param request
	 * @param response
	 * @param next
	 * @returns user || error
	 */
	async store(request: Request, response: Response, next: NextFunction) {
		const user = await UserRepositories.create(request.body)

		if (user.code) return next(new HandleErrors(user.message, user.code))

		return response.status(201).json(user)
	}

	/**
	 * Atualiza o usuário no banco
	 * @param request
	 * @param response
	 * @param next
	 * @returns user
	 */
	async update(request: Request, response: Response, next: NextFunction) {
		const user = {
			id: request.params.id,
			...request.body
		}

		const updateUser = await UserRepositories.update(user)

		if (updateUser.code) return next(new HandleErrors(updateUser.message, updateUser.code))

		return response.status(201).json(updateUser)
	}

	/**
	 * Deleta o usuário do banco
	 * @param request
	 * @param response
	 * @param next
	 * @returns
	 */
	async delete(request: Request, response: Response, next: NextFunction) {
		const { id } = request.params
		const user = await UserRepositories.delete(id)

		if (!user) return response.status(200).json({'message': 'User deleted with success'})

		return next(new HandleErrors(user.message, user.code))
	}
}

export default new UserController()
