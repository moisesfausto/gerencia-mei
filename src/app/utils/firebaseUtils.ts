import { initializeApp } from 'firebase/app'
import {
	deleteObject,
	getDownloadURL,
	getStorage,
	list,
	listAll,
	ref,
	uploadBytes,
	uploadBytesResumable } from 'firebase/storage'

const firebaseConfig = {
	apiKey: process.env.API_KEY,
	authDomain: `${process.env.PROJECT_ID}.firebaseapp.com`,
	databaseURL: `https://${process.env.F_DATABASE_NAME}.firebaseio.com`,
	projectId: process.env.PROJECT_ID,
	storageBucket: `${process.env.PROJECT_ID}.appspot.com`,
	messagingSenderId: process.env.SENDER_ID,
	appId: process.env.APP_ID,
	measurementId: `G-${process.env.MEASUREMENT_ID}`,
}

initializeApp(firebaseConfig)

export { getStorage, ref, listAll, list, uploadBytes, uploadBytesResumable, getDownloadURL, deleteObject }
