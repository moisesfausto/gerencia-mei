const formatToDateTime = (date: Date) => new Date(date)

const removeSpecialCharacter = (value: string, replace = '') => value.replace(/[^a-zA-Z0-9]/g, replace)

const formatFileName = (file: string): string => {
	// eslint-disable-next-line @typescript-eslint/no-unused-vars
	const [name, _] = file.split('.pdf')
	return removeSpecialCharacter(name, '-').toLowerCase()
}

const limitFileSize = (size: number): boolean => {
	const limit = 3000

	if ((size / 1024 / 1024) > limit) return false

	return true
}

export { formatToDateTime, removeSpecialCharacter, formatFileName, limitFileSize }
