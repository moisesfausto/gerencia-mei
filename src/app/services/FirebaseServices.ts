import moment from 'moment'
import {deleteObject, getDownloadURL, getStorage, list, ref, uploadBytes} from '@utils/firebaseUtils'

class FirebaseServices {
	private storage

	constructor() {
		this.storage = getStorage()
	}

	async uploadFile(idUser: string, idCompany: string, folder: string, file: IFile) {
		if (!file) return { message: 'File not exist' }

		const fileName = moment().format('YYYYMMDDThhmmss')

		const reference = ref(this.storage, `${idUser}/${idCompany}/${folder}/${fileName}`)

		try {
			await uploadBytes(reference, file.buffer)

			return {
				id_user: idUser,
				id_company: idCompany
			}
		} catch (error) {
			throw new Error('Failed to upload file')
		}
	}

	async listFile(idUser: string, idCompany: string, folder: string) {
		const listFiles: Array<string> = []

		const reference = ref(this.storage, `${idUser}/${idCompany}/${folder}`)

		const listFile = await list(reference, { maxResults: 36 })

		listFile.items.forEach(item => {
			listFiles.push(item.fullPath)
		})

		if (listFile.nextPageToken) {
			return await list(reference, {
				maxResults: 36,
				pageToken: listFile.nextPageToken,
			})
		}

		return {
			listFiles,
			items: listFiles.length
		}
	}

	async deleteFile(idUser: string, idCompany: string, folder: string, file: string) {
		const urlPath = `${idUser}/${idCompany}/${folder}/${file}`
		const reference = ref(this.storage, urlPath)

		try {
			await deleteObject(reference)

			return {
				message: 'File deleted successfully'
			}
		} catch (error: any) {
			return {
				message: error.message
			}
		}
	}

	async downloadFile(idUser: string, idCompany: string, folder: string, file: string) {
		const urlPath = `${idUser}/${idCompany}/${folder}/${file}`
		const reference = ref(this.storage, urlPath)

		try {
			return await getDownloadURL(reference)
		} catch (error: any) {
			return {
				message: error.message
			}
		}
	}

}

export default new FirebaseServices()
