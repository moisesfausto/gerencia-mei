import { HandleErrors } from '@helpers/HandleErrors'
import loggerService from '@services/LoggerService'
import {NextFunction, Request, Response} from 'express'

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const errorMiddleware = (error: Error & Partial<HandleErrors>, request: Request, response: Response, next: NextFunction) => {
	const statusCode = error.status ?? 500
	const message = error.status ? error.message : 'Internal Server Error'

	// Geração de log da aplicação
	loggerService.error({
		statusCode,
		message
	})
	// Geração de log da aplicação

	return response.status(statusCode).json({ message })
}

export default errorMiddleware
