import errorMiddleware from '@middlewares/errorMiddleware'

import express from 'express'
import { log } from 'console'

import { router } from './routes'

import 'express-async-errors'

const app = express()

app.use(express.json())
app.use(router)
app.use(errorMiddleware)

app.listen(process.env.APP_PORT, () => log(`🚀 server started at ${process.env.APP_URL_BASE}:${process.env.APP_PORT}`))
