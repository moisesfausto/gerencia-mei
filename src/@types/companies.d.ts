enum EType {
  MEI = 'MEI',
  ME = 'ME',
  EIRELI = 'EIRELI',
}

interface ICompany {
  id?: string,
  user_id: string
  cnpj: string,
  answerable: string,
  type: EType,
  section: string,
}
interface IDasCompany {
  id?: string,
  id_company: string,
  month: number,
  year: number,
  due_date: Date,
  value: number,
  amount_paid: number,
  situation: boolean,
  attachment: string,
}

interface IDasCompanyFile {
  fieldname: string,
  originalname: string,
  encoding: string,
  mimetype: string,
  buffer: string|any,
  size: number
}
