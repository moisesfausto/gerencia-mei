interface IMessageError {
	code: string
	email?: string
}

interface IPrismaErrors {
	code: string
	clientVersion: string
	meta: {
		field_name: string
	}
}

interface IError {
	code: number
	message: string
}
