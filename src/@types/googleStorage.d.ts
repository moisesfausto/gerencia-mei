interface IFile {
	fieldname: string,
	originalname: string,
	encoding: string,
	mimetype: string,
	buffer: string|any,
	size: number
}
