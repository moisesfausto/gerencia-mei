
# DOC Fácil

Completo sistema de gerenciamento de CNPJ para MEI e ME, onde o usuário pode gerencia toda a sua conta de forma prática e fácil.


## Download

Faça o clone do repo abaixo:

[![repo](https://img.shields.io/badge/Reposit%C3%B3rio-gitlab-orange)](https://gitlab.com/moisesfausto/gerencia-mei)

Caso precise de acesso, peça ao adm do sistema

## Instalação

Após o clone use os comandos abaixo para startar o docker

```bash
  cd gerencia-mei
  docker composer up -d
```

Em seguida, quando tiver já startado o docker e buildado a imagem, precisamos fazer o migrate das tabelas, para precisamos acessar a imagem do backend e depois fazer o migrate das tabelas

```bash
docker exec node_backend /bin/sh
```

Após estarmos na aplicação dentro do container, rode o comando:

```bash
yarn prisma migrate dev
```
## Variáveis de Ambiente

Para rodar esse projeto, você vai precisar adicionar as seguintes variáveis de ambiente no seu .env


~~~
DATABASE_URL="postgresql://<USER>:<PASSWORD>@<HOST>:<PORT>/<DATABASE>?schema=<SCHEMA>"

# Aplicação
APP_URL_BASE=
APP_PORT=

# Banco de dados
DATABASE_NAME=
DATABASE_PORT=
DATABASE_USER=
DATABASE_PASSWORD=

# Firebase
API_KEY=
PROJECT_ID=
F_DATABASE_NAME=
SENDER_ID=
APP_ID=
MEASUREMENT_ID=

SECRET_TOKEN=
~~~

Para gerar a secret acima, rode o seguinte script:

```console.log(require('crypto').randomBytes(20).toString('base64').replace(///g,'_').replace(/+/g,'-').toUpperCase())```

O que for gerado, copie e cole no SECRET_TOKEN



## Rodando localmente

O processo de instalação e build local é o mesmo do item de Instalação acima, porém, caso queira rodar só a API por fora do docker, rode o comando:

```bash
yarn dev
```
## Rodando os testes

Para rodar os testes, rode o seguinte comando

```bash
  yarn test
```


## Deploy

Para fazer o deploy desse projeto rode

```bash
  yarn prod
```


## Stack utilizada

**Front-end:**
![vue.js](https://img.shields.io/badge/Vue-20232A?logo=vue.js)
![tailwindcss](https://img.shields.io/badge/Tailwind_CSS-38B2AC?logo=tailwind-css&logoColor=white)

**Back-end:**
![nodejs](https://img.shields.io/badge/Node.js-43853D?logo=node.js&logoColor=white)
![express](https://img.shields.io/badge/express-404D59?logo=express)
![typescript](https://img.shields.io/badge/TypeScript-007ACC?logo=typescript&logoColor=white)
![multer](https://img.shields.io/badge/multer-000?logo=multer)
![firebase](https://img.shields.io/badge/Firebase-F29D0C?logo=firebase&logoColor=white)

**Banco de Dados:**
![prisma](https://img.shields.io/badge/prisma-474C55?logo=prisma)

**Test:**
![jest](https://img.shields.io/badge/jest-C22425?logo=jest)

**DevOps:**
![docker](https://img.shields.io/badge/Docker-2496ED?logo=docker&logoColor=white)

## Desenvolvedor

- [@moisesfauto](https://beacons.ai/moisesfausto)
