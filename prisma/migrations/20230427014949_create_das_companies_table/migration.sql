-- CreateTable
CREATE TABLE "das_companies" (
    "id" TEXT NOT NULL,
    "id_company" TEXT NOT NULL,
    "month" INTEGER NOT NULL,
    "year" INTEGER NOT NULL,
    "due_date" DATE NOT NULL,
    "value" REAL NOT NULL,
    "amount_paid" REAL NOT NULL,
    "situation" BOOLEAN NOT NULL,
    "attachment" VARCHAR(255) NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "das_companies_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "das_companies" ADD CONSTRAINT "das_companies_id_company_fkey" FOREIGN KEY ("id_company") REFERENCES "companies"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
