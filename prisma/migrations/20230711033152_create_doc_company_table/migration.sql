-- CreateTable
CREATE TABLE "doc_company" (
    "id" TEXT NOT NULL,
    "id_company" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    "attachment" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "doc_company_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "doc_company" ADD CONSTRAINT "doc_company_id_company_fkey" FOREIGN KEY ("id_company") REFERENCES "companies"("id") ON DELETE CASCADE ON UPDATE CASCADE;
