-- CreateTable
CREATE TABLE "irpf_declaration" (
    "id" TEXT NOT NULL,
    "id_company" TEXT NOT NULL,
    "year" INTEGER NOT NULL,
    "situation" BOOLEAN NOT NULL,
    "attachment" VARCHAR(255) NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "irpf_declaration_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "irpf_declaration" ADD CONSTRAINT "irpf_declaration_id_company_fkey" FOREIGN KEY ("id_company") REFERENCES "companies"("id") ON DELETE CASCADE ON UPDATE CASCADE;
