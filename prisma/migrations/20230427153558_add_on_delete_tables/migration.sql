-- DropForeignKey
ALTER TABLE "companies" DROP CONSTRAINT "companies_user_id_fkey";

-- DropForeignKey
ALTER TABLE "das_companies" DROP CONSTRAINT "das_companies_id_company_fkey";

-- AddForeignKey
ALTER TABLE "companies" ADD CONSTRAINT "companies_user_id_fkey" FOREIGN KEY ("user_id") REFERENCES "users"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "das_companies" ADD CONSTRAINT "das_companies_id_company_fkey" FOREIGN KEY ("id_company") REFERENCES "companies"("id") ON DELETE CASCADE ON UPDATE CASCADE;
