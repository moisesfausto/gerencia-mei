-- CreateTable
CREATE TABLE "bill_to_pay" (
    "id" TEXT NOT NULL,
    "id_das_company" TEXT NOT NULL,
    "id_irpf_declaration" TEXT NOT NULL,
    "title" TEXT NOT NULL,
    "value_to_pay" REAL NOT NULL,
    "amount_paid" REAL NOT NULL,
    "payment_date" DATE NOT NULL,
    "due_date" DATE NOT NULL,
    "situation" BOOLEAN NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "bill_to_pay_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "bill_to_pay" ADD CONSTRAINT "bill_to_pay_id_das_company_fkey" FOREIGN KEY ("id_das_company") REFERENCES "das_companies"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "bill_to_pay" ADD CONSTRAINT "bill_to_pay_id_irpf_declaration_fkey" FOREIGN KEY ("id_irpf_declaration") REFERENCES "irpf_declaration"("id") ON DELETE CASCADE ON UPDATE CASCADE;
